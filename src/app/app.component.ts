import {Component, OnInit} from '@angular/core';
import {Todo, TodoService} from './Services/todo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  todos: Todo[] = [];
  id = 200;
  filter = 'all';
  inputSearch: string;

  constructor(private todoService: TodoService) {
  }

  ngOnInit(): void {
    this.fetchTodo();
  }

  fetchTodo(){
    this.todoService.fetchTodo()
      .subscribe(todos => {
        this.todos = todos.map(todo => {
          return ({
            title: todo.title,
            completed: todo.completed,
            id: todo.id,
            userId: todo.userId,
            important: false
          });
        });
      });
  }

  filterTodo(): Todo[]{
    switch (this.filter) {
      case 'all':
        return this.todos;
      case 'completed':
        return this.todos.filter(item => item.completed);
      case 'important':
        return this.todos.filter(item => item.important);
      case 'active':
        return this.todos.filter(item => !item.completed);
      case 'search':
        const regex = new RegExp(`${this.inputSearch}`);
        return this.todos.filter(item => regex.test(item.title));
    }
  }

  onChangeImportant(todo: Todo){
    this.todos.find(item => item.id === todo.id).important = !todo.important;
  }

  onChangeCompleted(todo: Todo){
    if (todo.id > 200){
      this.todos.find(item => item.id === todo.id).completed = !todo.completed;
    }
    else{
      this.todoService.changeCompleted(todo)
        .subscribe(item => {
          this.todos.find(t => t.id === item.id).completed = item.completed;
        });
    }
  }

  onAddTodo(title: string){
    this.id++;
    this.todoService.addTodo({
      title,
      completed: false})
      .subscribe(() =>
      {
        this.todos.unshift({
          title,
          completed: false,
          id: this.id,
          important: false
        });
      });
  }

  onRemoveTodo(id: number) {
    this.todoService.removeTodo(id)
      .subscribe(() => {
        this.todos = this.todos.filter(item => item.id !== id);
      });
  }

  onSetFilter({filter, inputSearch}){
    this.filter = filter;
    this.inputSearch = inputSearch;
  }
}
