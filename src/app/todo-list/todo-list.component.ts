import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Todo, TodoService} from '../Services/todo.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
  providers: [TodoService]

})
export class TodoListComponent {
  @Input()
  todos: Todo[];

  @Output()
  changeCompleted: EventEmitter<Todo> = new EventEmitter<Todo>();

  @Output()
  changeImportant: EventEmitter<Todo> = new EventEmitter<Todo>();

  @Output()
  removeTodo: EventEmitter<number> = new EventEmitter<number>();

  onChangeImportant(todo: Todo){
    this.changeImportant.emit(todo);
  }

  onChangeCompleted(todo: Todo){
    this.changeCompleted.emit(todo);
  }

  onRemoveTodo(id: number){
    this.removeTodo.emit(id);
  }

}

