import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Todo} from '../Services/todo.service';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent{

  @Output()
  addTodo: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  setFilter: EventEmitter<any> = new EventEmitter<any>();

  inputSearch: string;
  filter = 'all';

  public form: FormGroup;
  constructor( private fb: FormBuilder) {
    this.form = fb.group({
      title: [null, Validators.compose([Validators.required, Validators.maxLength(50)])]
    });
  }

  onAddTodo(input: string){
    this.addTodo.emit(input);
    this.form.reset();
  }

  onSetFilter(filter: string){
    if (filter !== 'search'){
      this.filter = filter;
    }
    this.setFilter.emit({filter, inputSearch: this.inputSearch });
  }

}
