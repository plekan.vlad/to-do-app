import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface Todo {
  title: string;
  completed: boolean;
  important?: boolean;
  id?: number;
  userId?: number;
}
@Injectable({
  providedIn: 'root'
})

export class TodoService {

  constructor(private http: HttpClient) { }

  fetchTodo(): Observable<Todo[]>{
     return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos',
       {
         params: new HttpParams().set('_limit', '40')
       });
  }

  addTodo(todo: Todo): Observable<Todo>{
    return this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', todo);
  }

  changeCompleted(todo: Todo): Observable<Todo>{
    return this.http.put<Todo>(`https://jsonplaceholder.typicode.com/todos/${todo.id}`, {
      completed: !todo.completed
    });
  }

  removeTodo(id: number): Observable<void>{
    return this.http.delete<void>(`https://jsonplaceholder.typicode.com/todos/${id}`);
  }
}

